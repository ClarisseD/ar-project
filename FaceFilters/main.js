"use strict";



// SETTINGS of this demo:
const SETTINGS = {
  gltfModelURL: 'rabbitEar/rabbitEar.gltf',
  offsetYZ: [2, 0], // offset of the model in 3D along vertical and depth axis
  scale: 1
};

const SETTINGSBOWTIE = {
  gltfModelURL: 'bowtie/scene.gltf',
  offsetYZ: [-1, 0], // offset of the model in 3D along vertical and depth axis
  scale: 1
};

let THREECAMERA = null;


// build the 3D. called once when Jeeliz Face Filter is OK
function init_threeScene(spec, checkboxRabbit, checkboxBowtie){
  const threeStuffs = JeelizThreeHelper.init(spec, null);

  // IMPORT THE GLTF MODEL:
  // from https://threejs.org/examples/#webgl_loader_gltf
  const gltfLoader = new THREE.GLTFLoader();
  if(checkboxRabbit.checked){
    gltfLoader.load( SETTINGS.gltfModelURL, function ( gltf ) {

      {
        const skyColor = 0xB1E1FF;  // light blue
        const groundColor = 0xFFFFFF;  // brownish orange
        const intensity = 1;
        const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
        gltf.scene.add(light);
      }
  
      gltf.scene.frustumCulled = false;
      
      // center and scale the object:
      const bbox = new THREE.Box3().expandByObject(gltf.scene);
  
      // center the model:
      const centerBBox = bbox.getCenter(new THREE.Vector3());
      gltf.scene.position.add(centerBBox.multiplyScalar(-1));
      gltf.scene.position.add(new THREE.Vector3(0,SETTINGS.offsetYZ[0], SETTINGS.offsetYZ[1]));
  
      // scale the model according to its width:
      const sizeX = bbox.getSize(new THREE.Vector3()).x;
      gltf.scene.scale.multiplyScalar(SETTINGS.scale / sizeX);
  
      // dispatch the model:
      threeStuffs.faceObject.add(gltf.scene);
    } ); //end gltfLoader.load callback
  }
  
  if(checkboxBowtie.checked){
    gltfLoader.load( SETTINGSBOWTIE.gltfModelURL, function ( gltf ) {

      {
        const skyColor = 0xB1E1FF;  // light blue
        const groundColor = 0xFFFFFF;  // brownish orange
        const intensity = 1;
        const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
        gltf.scene.add(light);
      }
  
      gltf.scene.frustumCulled = false;
      
      // center and scale the object:
      const bbox = new THREE.Box3().expandByObject(gltf.scene);
  
      // center the model:
      const centerBBox = bbox.getCenter(new THREE.Vector3());
      gltf.scene.position.add(centerBBox.multiplyScalar(-1));
      gltf.scene.position.add(new THREE.Vector3(0,SETTINGSBOWTIE.offsetYZ[0], SETTINGSBOWTIE.offsetYZ[1]));
  
      // scale the model according to its width:
      const sizeX = bbox.getSize(new THREE.Vector3()).x;
      gltf.scene.scale.multiplyScalar(SETTINGSBOWTIE.scale / sizeX);
  
      // dispatch the model:
      threeStuffs.faceObject.add(gltf.scene);
    } ); //end gltfLoader.load callback
  }
  
  
  //CREATE THE CAMERA
  THREECAMERA = JeelizThreeHelper.create_camera();
} //end init_threeScene()

//entry point, launched by body.onload():
function main(){
  JeelizResizer.size_canvas({
    canvasId: 'jeeFaceFilterCanvas',
    isFullScreen: true,
    callback: start,
    onResize: function(){
      JeelizThreeHelper.update_camera(THREECAMERA);
    }
  })
}

function start(){

const checkboxRabbit = document.querySelector('input[value="rabbitEar"]');
const checkboxBowtie = document.querySelector('input[value="bowtie"]');

  JEEFACEFILTERAPI.init({ 
    videoSettings:{ // increase the default video resolution since we are in full screen
      'idealWidth': 1280,  // ideal video width in pixels
      'idealHeight': 800,  // ideal video height in pixels
      'maxWidth': 1920,    // max video width in pixels
      'maxHeight': 1920    // max video height in pixels
    },
    followZRot: true,
    canvasId: 'jeeFaceFilterCanvas',
    NNCPath: '../neuralNets/', //root of NN_DEFAULT.json file
    callbackReady: function(errCode, spec){
      if (errCode){
        console.log('AN ERROR HAPPENS. SORRY BRO :( . ERR =', errCode);
        return;
      }

      console.log('INFO: JEEFACEFILTERAPI IS READY');
      init_threeScene(spec, checkboxRabbit, checkboxBowtie);
    }, //end callbackReady()

    // called at each render iteration (drawing loop):
    callbackTrack: function(detectState){
      checkboxBowtie.onchange = function(){
        document.location.reload();
      }
      checkboxRabbit.onchange = function(){
        document.location.reload();
      }

      JeelizThreeHelper.render(detectState, THREECAMERA);
    }
  }); //end JEEFACEFILTERAPI.init call
} //end start()

